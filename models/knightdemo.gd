extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var X = Vector3(1,0,0)
var Y = Vector3(0,1,0)
var Z = Vector3(0,0,1)

func get_pos():
  return global_transform

var direction 	= Vector3(0,0,0)
var inputVector = null
# Called when the node enters the scene tree for the first time.
func _ready():
  $AnimationPlayer.get_animation("Run").loop = true
  $AnimationPlayer.get_animation("idle").loop = true
  $AnimationPlayer.play("idle")

var speed = 0.02
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
  if Input.is_action_pressed("ui_up"):
    rotation.y = 3*PI/2
  if Input.is_action_pressed("ui_down"):
    rotation.y = PI/2
  if Input.is_action_pressed("ui_left"):
    rotation.y = 0
  if Input.is_action_pressed("ui_right"):
    rotation.y = PI
    $AnimationPlayer.play("idle")
    translate_object_local(Vector3(0,0,speed))
  if inputVector:
    if $AnimationPlayer.get_current_animation() == "idle":
      $AnimationPlayer.play("Run")
    var dir = 0.001*Vector3(inputVector.x,0,inputVector.y)
    rotation.y = 3*PI/2
    translate(dir)
    rotation.y = PI - Vector2(-dir.x,-dir.z).angle()
  else:
    if $AnimationPlayer.get_current_animation() != "idle":
      $AnimationPlayer.play("idle")

func handle_input_vector(vec):
  if vec == Vector2(0,0):
    inputVector = null
  else:
    inputVector = vec
  

