extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const BASETILESIZE = 2.4

const _hexLib = preload("../helpers/hex_lib.gdns") # Relative path
const _hexLib2 = preload("../helpers/hex_lib.gd") # Relative path
const _hexExtras = preload("../helpers/hex_extras.gd") # Relative path
onready var HexLib = _hexLib.new()
onready var HexLib2 = _hexLib2.new()

onready var HexExtras = _hexExtras.new()

var tileSize = BASETILESIZE

const RAY = 4

var map_scale = 1.52
var currentMonster = null
var players = {}

var debug = false

func find_player(name):
  for player in players.values():
    if player.name == name:
      return player



func setMonster(monster):
  currentMonster = monster

func move_player(player,coord):
  player.transform.origin = HexLib.hex_to_worldXYZ(coord*map_scale)
  # we make the player touch the ground
  player.transform.origin.y -= player.feet_offset*map_scale
  player.coord = coord
  if currentMonster:
    player.transform = player.transform.looking_at(currentMonster.transform.origin, Vector3.UP)
    player.rotation.x = 0
    player.rotation.y = PI + player.rotation.y
    player.rotation.z = 0
    print("rotated player")
  
func move_player_ahead(player,coord,position):
  player.transform.origin = position
  player.coord = coord
  if currentMonster:
    player.transform = player.transform.looking_at(currentMonster.transform.origin, Vector3.UP)
    player.rotation.x = 0
    player.rotation.y = PI + player.rotation.y
    player.rotation.z = 0
    print("rotated player")
  
func add_player(coord,player):
  if player:
    player.map = self
    players[coord] = player
    print("added")
    move_player(player,coord)
    
    print("moved player")


func mel(c):
  HexLib.show_directions(c)
  HexLib2.show_directions(c)
# Called when the node enters the scene tree for the first time.
func _ready():
  tileSize = $stock/tile.scale.x * BASETILESIZE
  HexLib.set_size(tileSize)
  HexExtras.set_size(tileSize)
  $multiTiles.transform = $multiTiles.transform.scaled(Vector3(map_scale,map_scale,map_scale))
  $multiDebug.transform = $multiTiles.transform




func get_nHex(spatial: Spatial):
  return HexLib.worldXYZ_to_hex(spatial.transform.origin)

func tile_at(hex):
  for child in $tiles.get_children():
    if HexLib.worldXYZ_to_hex(child.transform.origin) == hex:
      return child
  
  
func cullMap():
  $multiTiles.multimesh.instance_count=0
  for child in $tiles.get_children():
    var c1 = HexLib.worldXYZ_to_hex(child.transform.origin)
    var c2 = HexLib.worldXYZ_to_hex(currentMonster.transform.origin)
    if HexLib.cube_distance(c1,c2) > 5:
      child.queue_free()


func recenterMap():
  if currentMonster:
    var i = 0
    var discCoords = HexLib.make_disc_from(
        HexLib.get_hex_coords(currentMonster),RAY)
    
    
    $multiTiles.multimesh.instance_count= discCoords.size()
    $multiTiles.multimesh.visible_instance_count= discCoords.size()
    if debug:
      for child in $multiDebug.get_children():
        if child.visible:
          child.queue_free()
    for coord in discCoords:
      if debug:
        var debugDisplay = $multiDebug/Debug.duplicate()
        debugDisplay.visible = true
        debugDisplay.position= 18.5*map_scale*Vector2(
            HexLib.hex_to_worldXYZ(coord).x,
            HexLib.hex_to_worldXYZ(coord).z)
        debugDisplay.get_node("Label").text = str(coord)
        $multiDebug.add_child(debugDisplay)
      $multiTiles.multimesh.set_instance_transform(i,
      Transform(Basis(),HexLib.hex_to_worldXYZ(coord)))
      i = i + 1
      colorMap()

func colorMap():
  var discCoords = HexLib.make_disc_from(HexLib.get_hex_coords(currentMonster),RAY)
  var count = $multiTiles.multimesh.instance_count
  var i = 0
  for coord in discCoords:
    var color = Color(0.5,0.5,0.6)
    if players.keys().has(coord):
      var player = players[coord]
      if player:
        color = player.color
    else :
      color = Color((count- i*2)/float(count), i*1.7/float(count),i*0.5/float(count))
    $multiTiles.multimesh.set_instance_color(i, color)
    i = i + 1
#		$stock/tile/Tile.material_override()
#	for tile in tiles.get_children():
#		tile.material.

# Called every frame. 'delta' is the elapsed time since the previous frame.
# warning-ignore:unused_argument
#func _process(_delta):
#    recenterMap()

func spawn_debug(where):
  var d = $debugS.duplicate()
  d.visible = true
  d.transform.origin = where
  add_child(d)

func get_pc_angle(playerC):
  var monsterC = HexLib.get_hex_coords(currentMonster)
  return HexLib.hex_to_planeXY(monsterC).angle_to_point( -HexLib.hex_to_planeXY(playerC))

func preview_vanish():
  $MoveEffect/Arrow.visible = false
  $MoveEffect/Target.visible = false
  
func preview_move(player,angle):
  var angVec = HexLib.cube_direction(
    HexLib.get_angle_direction(-angle-PI)
   )
  $MoveEffect/Arrow.visible = true
  $MoveEffect/Target.visible = true
  $MoveEffect/Arrow.rotation.y  = angle
  
  $MoveEffect/Arrow.transform.origin = player.transform.origin
  $MoveEffect/Target.transform.origin = map_scale*HexLib.hex_to_worldXYZ(angVec + player.coord)+ Vector3(0,-1,0)
#  $MoveEffect/Target.rotation.y  = angle



func get_swipe_coord(move,monsterC,playerC): 
  var angle = HexLib.hex_to_planeXY(monsterC).angle_to_point( -HexLib.hex_to_planeXY(playerC))
  var swipeAngle = move.angle()
  var ang = swipeAngle + angle
  var dir = HexLib.get_angle_direction(ang)
  var angVec = HexLib.cube_direction(dir)
  print(angVec)
  var worldMove = map_scale*HexLib.hex_to_worldXYZ(angVec + playerC)
  return angVec + playerC

func _on_player_moved(p,move):
  print(move)
  var center = HexLib.get_hex_coords(currentMonster)
  var new_coord = get_swipe_coord(move,center,p.coord)
  if new_coord != center :
    p.set_move_target(new_coord)

func _on_player_finished_run(p,new_coord,position):
  players.erase(p.coord)
  move_player_ahead(p, new_coord,position)
  players[new_coord] = p
  print("finished_run")
  p.back_to_normal()
  recenterMap()

func _on_player_moved_dir(p,dir):
  var new_coord = HexLib.cube_neighbor(p.coord,dir)
  print("old:", p.coord, " new:", new_coord)
  players.erase(p.coord)
  move_player(p, new_coord)
  players[new_coord] = p
  recenterMap()
