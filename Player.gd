extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

enum STATE {
  Idle,
  Running
 }

var coord         = Vector3()

var color         = Color(1,0.8,0.7)

var feet_offset   = 0.64
onready var y_offset = Vector3(0,-feet_offset,0)

var move_target   = [] # array containing : 
  # the hex coordinate of the target
  # the world coordinate of 

export var move_current  = 0.0

var move_total    = 0.6 #en secondes !

var map = null


var state = STATE.Idle

signal moved(player,move)
signal moved_dir(player,dir)
signal finished_run(player,target,wtarget)



# Called when the node enters the scene tree for the first time.
func _ready():
  if $AnimationPlayer:
    $AnimationPlayer.get_animation("RunForward").loop = true
    $AnimationPlayer.get_animation("Idle").loop = true
    $AnimationPlayer.play("Idle")
  pass # Replace with function body.

  

func move(move):
  emit_signal("moved",self,move)

func move_dir(dir):
  print(dir)
  emit_signal("moved_dir",self,dir)

func set_move_target(tcoord):
  state = STATE.Running
  move_target = [tcoord
                ,map.map_scale*y_offset+map.map_scale*map.HexLib.hex_to_worldXYZ(tcoord)
                ,map.map_scale*y_offset+map.map_scale*map.HexLib.hex_to_worldXYZ(coord)]
#  rotation.y = Vector2()
  transform = transform.looking_at(move_target[1], Vector3.UP)
  rotation.x = 0
  rotation.y = PI + rotation.y
  rotation.z = 0
  $AnimationPlayer.play("RunForward")
  


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
  if state == STATE.Running:
      var percent = move_current / move_total
      transform.origin = percent*move_target[1] + (1-percent)*move_target[2]

func finish_run():
  emit_signal("finished_run",self,move_target[0],move_target[1])

func back_to_normal():
  move_current = 0
  state = STATE.Idle
  $AnimationPlayer.play("Idle")
