extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var tileSize

# Called when the node enters the scene tree for the first time.
func _ready():
  pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#  pass

func set_size(size):
  tileSize = size

func cube_add(c1,c2):
  return c1 + c2
  
func cube_distance(a, b):
  return (abs(a.x - b.x) + abs(a.y - b.y) + abs(a.z - b.z)) / 2

var cube_directions = [
  Vector3(+1, -1, 0), Vector3(+1, 0, -1), Vector3(0, +1, -1), 
  Vector3(-1, +1, 0), Vector3(-1, 0, +1), Vector3(0, -1, +1)
]

func cube_direction(direction):
  if !direction == null:
    return cube_directions[direction % 6]
  else:
    return Vector3()
    
func cube_neighbor(cube, direction):
  return cube_add(cube, cube_direction(direction))

func cube_scale(c1,s):
  return s * c1

func show_directions(cube):
  print("center : ", cube)
  for i in range(0,6):
    print(i, " : ", cube_neighbor(cube,i))
    print(i, " : ", get_direction(cube_neighbor(cube,i),cube))
func get_hex_coords(spatial):
  return worldXYZ_to_hex(spatial.transform.origin)

func worldXYZ_to_hex(point):
  var q = (sqrt(3)/3 * point.x  -  1/3 * point.z) / tileSize
  var r = (2/3 * point.y) / tileSize
  return Vector3(q,-q-r,r)

func hex_to_worldXYZ(coord : Vector3):
  var q = coord.x
  var r = coord.z
  var x = tileSize * (sqrt(3) * q + sqrt(3)/2 * r)
  var y = tileSize * 3/2 * r
  return Vector3(x,0,y)
  

func hex_to_planeXY(coord : Vector3):
  var q = coord.x
  var r = coord.z
  var x = tileSize * (sqrt(3) * q + sqrt(3)/2 * r)
  var y = tileSize * 3/2 * r
  return Vector2(x,y)

func make_circle_from(center, radius):
  var results = []
  # this code doesn't work for radius == 0; can you see why?
  var cube = cube_add(center, cube_scale(cube_direction(4), radius))
  for i in range(0,6):
    for _j in range(0,radius):
      results.append(cube)
      cube = cube_neighbor(cube, i)
  return results

func make_disc_from(center, radius):
  var results = [center]
  for i in range(0,radius+1):
    results+= make_circle_from(center,i)
  return results



func get_angle_direction(orAngle):
  var angle = fmod((PI + 2*PI/3 - orAngle),2*PI)
  print("actual angle:", rad2deg(angle- 2*PI/3))
  if 0 <= angle and angle < PI/3:
    return 0
  elif angle >= PI/3 and angle < 2*PI/3:
    return 1
  elif angle >= 2*PI/3 and angle < PI:
    return 2
  elif angle >= PI and angle < 4*PI/3:
    return 3
  elif angle >= 4*PI/3 and angle < 5*PI/3:
    return 4
  else:
    return 5

func get_direction(c1,c2):
  if c1.x > c2.x:
    if c1.z < c2.z:
      return 1
    elif c1.y < c2.y:
      return 0
  if c1.y > c2.y:
    if c1.x < c2.x:
      return 3
    elif c1.z < c2.z:
      return 2
  if c1.z > c2.z:
    if c1.y < c2.y:
      return 5
    elif c1.x < c2.x:
      return 4



