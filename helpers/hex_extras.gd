extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const _hexLib = preload("../helpers/hex_lib.gd") # Relative path
onready var HexLib = _hexLib.new()

# Called when the node enters the scene tree for the first time.
func _ready():
  pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#  pass

var tileSize

func set_size(size):
  tileSize = size

func get_swipe_coord(move,playerC,monsterC): 
  var angle = HexLib.hex_to_planeXY(playerC).angle_to(HexLib.hex_to_planeXY(monsterC))
  print(angle)

