extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


var start = null
var player = null
var angle = 0
var slow = true


# Called when the node enters the scene tree for the first time.
func _ready():
  pass
  
  
  
func _input(event):
  if (event is InputEventScreenDrag) or (event is InputEventScreenTouch) and event.is_pressed():
    if !start:
      angle = -player.map.get_pc_angle(player.coord)
#      $HexIndic.position = event.position
#      $WhereIndic.position = event.position
#      $HexIndic.visible = true
#      $HexIndic.rotation = angle
      start = event.position
    elif slow :
      var dirangle  = (event.position - start).angle()
      var vecl      = (event.position - start).length()
#      $WhereIndic.points[-1] = vecl*Vector2(cos(dirangle),sin(dirangle))
      if vecl > 30:
        player.map.preview_move(player, -dirangle+angle)
      else:
        player.map.preview_vanish()
      slow = false
    else:
      slow = true
  elif (event is InputEventScreenDrag) or (event is InputEventScreenTouch) and !event.is_pressed():
    var end  = event.position
    var vecl      = (end - start).length()
    if start and vecl > 10:
      player.map.preview_vanish()
#      $HexIndic.visible = false
#      $WhereIndic.visible = false
#      $HexIndic.rotation = 0
      player.move(start - end)
      angle = 0
      start = null
      
  pass # Replace with function body.

