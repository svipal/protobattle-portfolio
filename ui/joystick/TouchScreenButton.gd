extends TouchScreenButton

var radius = shape.radius
var boundary = 0
var center = Vector2(0,0)
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func get_bpos():
  return position + global_scale*0.5*Vector2(12,12)

# Called when the node enters the scene tree for the first time.
func _ready():
  
  center = get_parent().global_position
  boundary = $Area2D/BackShape.shape.radius * global_scale
  pass # Replace with function body.

func _input(event):
  if (event is InputEventScreenDrag) or (event is InputEventScreenTouch) and event.is_pressed():
    var newpos = event.position - radius*global_scale
    var vecToCenter = (newpos - center + global_scale*Vector2(12,12)*0.5)
    var distanceToCenter = vecToCenter.length() 
    if distanceToCenter < boundary.length():
      set_global_position(event.position - radius*global_scale)
    elif distanceToCenter < 2*boundary.length():
      set_global_position(center + vecToCenter.normalized() * boundary - global_scale*Vector2(12,12)*0.5)
  elif (event is InputEventScreenDrag) or (event is InputEventScreenTouch) and !event.is_pressed():
    set_global_position(center- global_scale*Vector2(12,12)*0.5)
#

  
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
