extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var main_player = Vector3(3,-1,-2)

func invX(vec):
  return Vector3(-vec.x,vec.y,vec.z)

# Called when the node enters the scene tree for the first time.
func _ready():
  
#	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
  $Map.setMonster($Guacanos/guac)
  
  
  for child in $Guacanos.get_children():
    child.get_child(1).get_animation("walk").loop=true
    child.get_child(1).play("walk")

  for child in $Dragons.get_children():
    child.get_child(3).get_animation("Fly").loop=true
    child.get_child(3).play("Fly")
    
  $Map.add_player(main_player,$Players/Knight)
  $Map.recenterMap()
  #We let the map know when the players have moved
  $Players/Knight.connect("moved",$Map,"_on_player_moved")
  $Players/Knight.connect("finished_run",$Map,"_on_player_finished_run")
  $Players/Knight.connect("moved_dir",$Map,"_on_player_moved_dir")
  #We need to know as well
  $Players/Knight.connect("finished_run",self,"_on_player_finished_run")
  $UI/SwipeHandler.player=$Players/Knight
  center_cam()

func center_cam():
  var p = $Map.find_player("Knight")
  if p:
    print("player found")
    var place = p.transform.origin
    $CloseCam.transform.origin= p.transform.origin 
    $CloseCam.rotation = p.rotation
    $CloseCam.rotate_y(PI)
    $CloseCam.translate(Vector3(0,2.6,6.5))
var move_vec := Vector3()
var mov = 0.1
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	var inputVector = -$UI/joystickMove/TouchScreenButton.get_bpos() 
  if Input.is_action_pressed("ui_down"):
    $CloseCam.make_current()
  if Input.is_action_pressed("ui_up"):
    $CamHandler/OverCam.make_current()
#	$Camera.transform.origin= $knight.transform.origin + Vector3()
  var caminf = 0.1*Vector3($UI/joystickCam/TouchScreenButton.get_bpos().y,$UI/joystickCam/TouchScreenButton.get_bpos().x,0) 
#
#	$Camera.translate(Vector3(0,1,5))
  $CloseCam.rotation +=caminf*delta
  
func _on_player_finished_run(player,_c,_w):
  call_deferred("center_cam")
